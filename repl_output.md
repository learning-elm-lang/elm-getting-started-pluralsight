# Elm: Getting Started

This is sample code for video course from Pluralsight *Elm: Getting Started*

## Values

### Strings

    > "hello"
    "hello" : String

    > "hello" ++ "world"
    "helloworld" : String

    > "hello" ++ " world"
    "hello world" : String

### Numbers

    > 1
    1 : number

    > 1 + 2 * 4
    9 : number

    > (1 + 2) * 4
    12 : number

## Functions

    > divideByTwo n = \
    |   n / 2
    <function> : Float -> Float

    > divideByTwo 6
    3 : Float
    > divideByTwo 9.0
    4.5 : Float

### Demo: creatng functions

Following code can be pasted into [Elm online editor](http://elm-lang.org/try) and compiled:

    import Html exposing (text)

    main =
        divideNumber 5 135
            |> text

    divideNumber divisor dividend =
        dividend / divisor
            |> toString

### Demo: function signatures

Following code can be pasted into Elm online editor and compiled.

    import Html exposing (text)

    main =
        divideBy5 135
            |> text

    divideNumber: Float -> Float -> String
    divideNumber divisor dividend =
        dividend / divisor
            |> toString

    divideBy5: Float -> String
    divideBy5 =
        divideNumber 5

Last function is an example of function currying.

## If expressions

    > isFactor dividend divisor = \
    |   if rem dividend divisor == 0 then True else False
    <function> : Int -> Int -> Bool

    > isFactor 4 2
    True : Bool

    > isFactor 5 2
    False : Bool

## Data Structures

Elm have 3 main data strctures: *List*, *Tuple* and *Record*.

### List

List can hold items of the same type:

    > primes = [2, 3, 5, 7]
    [2,3,5,7] : List number

    > strPrimes = ["2", "3", "5", "7"]
    ["2","3","5","7"] : List String

### Tuples

Tuple can combine items of different types:

    > calculationResult = (True, 42)
    (True,42) : ( Bool, number )

### Records

    > point = { x = 3, y = 14 }
    { x = 3, y = 14 } : { x : number, y : number1 }

### Demo: Lists

Following code can be pasted into [Elm online editor](http://elm-lang.org/try) and compiled:

    import Html exposing (text)
    import List

    funWithLists =
    let
        myList = [1, 2, 3]
    in
        List.reverse myList
            |> toString

    main =
        text funWithLists

## Demo: Tuples

Following code can be pasted into [Elm online editor](http://elm-lang.org/try) and compiled:

    import Html exposing (text)
    import Tuple

    main =
        -- text funWithTuples
        let
            -- result = (True, "The result")
            result = (False, "The result")
        in
            text <|
                if (Tuple.first result) then
                    "Success: " ++ Tuple.second result
                else
                    "Failure: " ++ Tuple.second result

    funWithTuples =
        let
            myTuple = ("First", 2)
        in
            toString myTuple

## Demo Records

Following code can be pasted into [Elm online editor](http://elm-lang.org/try) and compiled:

    import Html exposing (text)

    type alias Point =
        { x : Float
        , y : Float
        }

    main =
        let
            point1 = { x = 1, y = 2 }
            point2 = { y = 10, x = 5 }
        in
            distanceBetween point1 point2
                |> toString
                |> text

    distanceBetween: Point -> Point -> Float
    distanceBetween p1 p2 =
        ((p1.x - p2.x) ^ 2 + (p1.y - p2.y) ^ 2) ^ 0.5

### Demo: Working with Messages

Following code can be pasted into [Elm online editor](http://elm-lang.org/try) and compiled:

import Html exposing (..)
import Html.Events exposing (onClick)

model = 0

    view model =
        div []
            [ button [ onClick Decrement ] [ text "-" ]
            , div [] [ text (toString model) ]
            , button [ onClick Increment ] [ text "+" ]
        ]

    type Message = Increment | Decrement

    update msg model =
        case msg of
            Increment ->
            model + 1

            Decrement ->
            model - 1

    main =
        beginnerProgram
            { model = model
            , view = view
            , update = update
            }

### Demo: Commands

Following code can be pasted into [Elm online editor](http://elm-lang.org/try) and compiled:

    import Html exposing (..)
    import Html.Attributes exposing (..)
    import Html.Events exposing (..)
    import Http
    import Json.Decode as Decode

    main =
    Html.program
        { init = init "cats"
        , view = view
        , update = update
        , subscriptions = subscriptions
        }

    -- MODEL
    type alias Model =
        { topic : String
        , gifUrl : String
        }


    init : String -> (Model, Cmd Msg)
    init topic =
        ( Model topic "waiting.gif"
        , getRandomGif topic
        )

    -- UPDATE
    type Msg
        = MorePlease
        | NewGif (Result Http.Error String)

    update : Msg -> Model -> (Model, Cmd Msg)
    update msg model =
        case msg of
            MorePlease ->
                (model, getRandomGif model.topic)

            NewGif (Ok newUrl) ->
                (Model model.topic newUrl, Cmd.none)

            NewGif (Err _) ->
                (model, Cmd.none)

    -- VIEW
    view : Model -> Html Msg
    view model =
        div []
            [ h2 [] [text model.topic]
            , button [ onClick MorePlease ] [ text "More Please!" ]
            , br [] []
            , img [src model.gifUrl] []
            ]

    -- SUBSCRIPTIONS
    subscriptions : Model -> Sub Msg
    subscriptions model =
        Sub.none

    -- HTTP
    getRandomGif : String -> Cmd Msg
    getRandomGif topic =
        let
            url =
                "https://api.giphy.com/v1/gifs/random?api_key=dc6zaTOxFJmzC&tag=" ++ topic
        in
            Http.send NewGif (Http.get url decodeGifUrl)

    decodeGifUrl : Decode.Decoder String
    decodeGifUrl =
        Decode.at ["data", "image_url"] Decode.string
